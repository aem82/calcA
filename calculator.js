var operandA = -1
var operandB = -1
var result = 0

document.addEventListener('DOMContentLoaded', function() {
	var numEls = document.querySelectorAll('.number');
	console.log(numEls);
	for (var i = 0; i < numEls.length; i++) {
		console.log(numEls[i]);
		var myNum = numEls[i].id;
		var myHandler = number.bind(null, myNum);
		numEls[i].addEventListener('click', myHandler);
	}
});


function number(x) {
	if (operandA == -1) {
		operandA = x;
		document.getElementById("result").innerHTML = operandA.toString()
	} else {
		operandB = x;
		document.getElementById("result").innerHTML = operandB.toString()
	}
}


function add() {
	if (operandA != -1 && operandB != -1) {
		result = operandA + operandB;
	}
}

function minus() {
	if (operandA != -1 && operandB != -1) {
		result = operandA - operandB;
	}
}

function multiply() {
	if (operandA != -1 && operandB != -1) {
		result = operandA * operandB;
	}
}

function divide() {
	if (operandA != -1 && operandB != -1) {
		result = operandA / operandB;
	}
}

function calculate() {
	document.getElementById("result").innerHTML = result.toString()
}